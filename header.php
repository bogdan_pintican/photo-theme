<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes() ?>><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes() ?>><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" <?php language_attributes() ?>><!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ) ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width">
        <link rel="icon" type="image/png" href="<?php bloginfo("template_url"); ?>/flavicon.ico">
        <title><?php wp_title( '|', true, 'right' ) ?> <?php bloginfo("name"); ?> </title>
		<meta name="author" content="">
		<link rel="author" href="">
        <!-- Fonts -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css">
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900' rel='stylesheet' type='text/css'>
		<?php wp_head() ?>


		<link rel="stylesheet" href="<?php bloginfo("template_url") ?>/css/foundation.css">
		<link rel="stylesheet" href="<?php bloginfo("template_url") ?>/style.css">
    </head>
    <body <?php body_class() ?>>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>


	<div class="social-links">
		<?php  get_template_part( 'social', 'links' ); ?>
	</div>

				
	<header id="page-header">
		<div class="row">
			<div class="header-top medium-12 columns">
				<?php if ( ! dynamic_sidebar("language_widget") ){} ?>
				<div class="fb-like" data-href="https://www.facebook.com/cristianioan.photo" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
			</div>

			<div class="logo medium-12 columns">
				<h1>
					<!-- <a href="<?php bloginfo("url"); ?>">Cristian Ioan Photography</a> -->
					<a href="<?php bloginfo("url"); ?>"><img src="<?php echo get_option('radu_logo') ?>" alt="Logo"></a>
				</h1>
			</div>
			<nav class="navigation small-12 columns">
				<?php wp_nav_menu(array(
					'theme_location' => 'primary',
					"depth" => "1"
				)) ?>
			</nav>

		</div>
	</header>