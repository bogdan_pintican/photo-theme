<?php get_header(); the_post(); ?>
<div class="main-content">
	<div class="row">
		<div class="medium-12 columns">
				<?php the_content(); ?>
		</div>
	</div>
</div>
<!-- <?php get_sidebar(); ?> -->
<?php get_footer(); ?>
