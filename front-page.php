<?php get_header() ?>

<!-- <div class="featured">
	<img src="<?php bloginfo("template_url"); ?>/img/cristi.jpg" alt="">
</div>
 -->

<?php the_post() ?>

<div class="main-content">
	
	<!-- 
					Galery Construction
	 -->

		<!-- Menu Construct -->
		<div class="galery-navigation row">
			<div class="small-12 columns">
				<ul>
					<?php
					$activeClass;
					$args = array('post_type'=>'page','posts_per_page'=>-1,'post_parent'=>get_the_ID(),'orderby'=>'menu_order','order'=>'ASC');
					$all_pages = new WP_Query($args);
					$i=0;
					while ($all_pages->have_posts()): $all_pages->the_post(); $i++; ?>
						<li>
							<button class="title <?php if( !isset($activeClass) ): echo "active"; endif; $activeClass = false;?>" value="#<?php echo $post->ID ?>"><?php echo the_title(); ?></button>
						</li>
					<?php 	endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</div><!-- Galery Navigation -->
		<div class="galery-body">
			<?php
			$activeClass;
			$args = array('post_type'=>'page','posts_per_page'=>-1,'post_parent'=>get_the_ID(),'orderby'=>'menu_order','order'=>'ASC');
			$all_pages = new WP_Query($args);
			$i=0;	
			while ($all_pages->have_posts()): 
				$all_pages->the_post(); 
				$i++; 
			?>
				<div id="<?php echo $post->ID ?>" class="galery-section <?php if( !isset($activeClass) ): echo "active"; endif; $activeClass = false; ?>">
					<?php the_content() ?>
				</div> <!-- Galery Section -->
			<?php 	endwhile; wp_reset_query(); ?>
		</div><!-- Galery Body -->


<div class="lightBox">
	<div class="spinner">
	  <div class="double-bounce1"></div>
	  <div class="double-bounce2"></div>
	</div>
	<div class="arrows">
		<button class="arrow-left" value="-1"><i class="fa fa-arrow-left"></i></button>
		<button class="arrow-right" value="1"><i class="fa fa-arrow-right"></i></button>
	</div>
</div>

</div>



<?php get_footer() ?>