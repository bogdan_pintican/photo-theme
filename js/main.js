

// document.addEventListener("contextmenu", function(e){
//     if(e.target.tagName === "IMG"){
//         e.preventDefault();
//         console.log("click");
//         return false;
//     }
// });

var $ = jQuery;

jQuery(document).ready(function ($) {

    var windowHeight = $(window).height(),
        documentHeight = $(document).height();

    $(window).css("min-height", documentHeight + "px");

$(document).foundation();


    /*==================================
    =            Masonry               =
    ==================================*/

    var $container = $('.gallery-wrapper');


    $("img.lazy").lazyload();

    // initialize Masonry after all images have loaded  
    // $container.imagesLoaded( function() {
    // });
      
    $container.masonry({});
    $container.find("img").on("load", function(){
        $container.masonry({});
        // console.log("image loaded");
    });  


    $(window).load(function(){
        if( $(".parteneri").length ){
            $(".parteneri").masonry();
        }
    });

    /*==================================
    =       Tab functionality         =
    ==================================*/
    var activeTab = $(".galery-navigation").find("button.active").attr("href"),
        $tabs = $(".galery-navigation").find("button");

    $(".galery-navigation").on("click", "button", function(event){
        event.preventDefault();

        var $target = $(this),
            tab = $target.val();

            activeTab = tab;

        // hide all tabs
        $(".galery-section")
            .removeClass("active")
            .fadeOut()
            .hide();


        $(".galery-navigation button").removeClass("active");

        // show the apropriate one
        $(tab).addClass("active");
        $target.addClass("active");

        $(tab).fadeIn();
        // reset the galery 
        $container.masonry();
        // load again
        $("img.lazy").lazyload();

        return false;
    });

    $(".galery-section").first().show();


    /*========================
    =       Light box         =
    ========================*/


    $(".galery-section").each(function(){
        $(this).magnificPopup({
            type:'image',
            delegate: "a",
            gallery: {enabled: true},
            removalDelay: 300,
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out'
            },
            opener: function(openerElement){
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        });
    });



    /*==============================
    =            Social            =
    ==============================*/

    $(window).on("scrollStop", function(){
        var $self = $(this);

        $(".social-links").css({
            transform: "translateY(" + $self.scrollTop()  +  "px)"
        });
    });

    var t;

    $(window).on("scroll", function(){

        var $self = $(this);

        if(t){
            clearTimeout(t);
        }

        t = setTimeout(function(){
            $self.trigger("scrollStop");
        }, 250);


    });


    /*==================================
    =            Equal height            =
    ==================================*/


    // var heightsFooterItem = $.map($(".footer-item"), function(el){
    //     return $(el).height();
    // });
    // var heighestHeightFooterItem = Math.max.apply(null, heightsFooterItem);
    // $(".footer-item").css("min-height", heighestHeightFooterItem + "px");



});