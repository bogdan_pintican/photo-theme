<?php

global $radu_filters;
if ( !isset( $radu_filters ) ) $radu_filters = new radu_filters();

class radu_filters {

	public function __construct() {

		// content formater 
		remove_filter( 'the_content', 'wpautop' );
		remove_filter( 'the_content', 'wptexturize' );
		add_filter( 'the_content', array( $this, 'formatter' ), 99 );
		add_filter( 'widget_text', array( $this, 'formatter' ), 99 );

		// disable admin bar
		add_filter( 'show_admin_bar', '__return_false' );

		// attachments
		add_filter( 'attachments_default_instance', '__return_false' ); 
		add_filter( 'attachments_settings_screen', '__return_false' );

		// white label
		//add_filter( 'admin_footer_text',array( $this, 'backend_footer' ) );

		// remove attachments dimmentions
		add_filter( 'post_thumbnail_html', array( $this, 'attachments_dimensions' ) );
		add_filter( 'image_send_to_editor', array( $this, 'attachments_dimensions' ) );

		// excerpt
		add_filter( 'excerpt_length', array( $this, 'excerpt_length' ) );
	}

	public function formatter( $content ) {
		$new_content = '';
		$pattern_full = '{(\[raw\].*?\[/raw\])}is';
		$pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
		$pieces = preg_split( $pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE );
		foreach ($pieces as $piece) {
			if ( preg_match( $pattern_contents, $piece, $matches ) ) {
				$new_content .= $matches[1];
			} else {
				$new_content .= wptexturize( wpautop( $piece ) );
			}
		}
		return $new_content;
	}

	public function backend_footer() {
		echo 'Theme developed by <a href="http://radudragomir.com" target="_blank">Radu Dragomir</a>';
	}

	public function attachments_dimensions( $html ) {
		$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
		return $html;
	}

	public function excerpt_length ( $length ) {
		global $post;
		if (is_front_page()) {
			if ($post->post_type == 'event') return 15;
			if ($post->post_type == 'post') return 15;
		}
		return 50;
	}

}