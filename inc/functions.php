<?php

class rdfw {

	public static function get_random_products( $count = 1 ) {
		global $post;
		$args = array(
			'post_type'=>'product',
			'posts_per_page' => $count,
			'orderby' => 'rand',
		);
		$products = new WP_Query( $args );
		if ( $count == 1 ) {
			$products->the_post();
			$ret = $post;
		} else {
			$ret = array();
			while ( $products->have_posts() ) {
				$products->the_post();
				$ret[] = $post;
			}
		}
		wp_reset_query();
		return $ret;
	}

	public static function is_set_home( $page_id ) {
		$frontpage_id = get_option( 'page_on_front' );
		return $frontpage_id==$page_id ? true : false;
	}

	public static function get_home_page() {
		return get_option( 'page_on_front' );
	}

	public static function get_blog_page() {
		return get_option( 'page_for_posts' );
	}

	public static function print_value( $value, $wrap, $apply_filters ) {
		if ( $value ) {
			$value = nl2br( stripslashes( $value ) );
			if ( !empty( $apply_filters ) ) {
				foreach ( $apply_filters as $key => $filter ) {
					$value = apply_filters( $filter, $value );
				}
			}
			if ( $wrap ) {
				$value = sprintf( $wrap, $value );
			}
		}
		echo $value;
	}

	public static function print_option( $option, $wrap = null, $apply_filters = array() ) {
		$value = get_option( 'radu_'.$option );
		self::print_value( $value, $wrap, $apply_filters );
	}

	public static function print_meta( $meta, $post_id = null, $wrap = null, $apply_filters = array(), $single = true ) {
		global $post;
		$post_id = $post_id ? $post_id : $post->ID;
		$values = get_post_meta( $post_ID, '_radu_'.$option, $single );
		if ($single) {
			self::print_value( $values, $wrap, $apply_filters );
		} else {
			foreach ($values as $key => $value) {
				self::print_value( $value, $wrap, $apply_filters );
			}
		}
	}

	public static function get_sliders() {
		global $wpdb;
		$sql = "SELECT * from {$wpdb->prefix}revslider_sliders order by title";
		return $wpdb->get_results($sql);
	}

	public static function count_active_sidebars($slug,$max) {
		$ret = 0;
		for($i=1;$i<=$max;$i++) {
			if (is_active_sidebar($slug.$i)) $ret++;
		}
		return $ret;
	}

	public static function get_page_by_template( $template, $single=true ) {
		global $wpdb;
		$sql = "SELECT *
				from {$wpdb->posts} as p
				left join {$wpdb->postmeta} as pm on
					p.ID = pm.post_id and
					pm.meta_key = '_wp_page_template'
				where pm.meta_value = '{$template}'
				and p.post_status = 'publish'
		";
		if ( $single ) $sql .= " limit 1";
		return $single ? $wpdb->get_row( $sql ) : $wpdb->get_results( $sql );
	}

	public static function get_term_image($id,$size='full',$return='url') {
	    global $wpdb;
	    $wpdb->show_errors();
	    $sql = "
	        select ID
	        from {$wpdb->posts} as p
	        left join ({$wpdb->term_relationships} as tr
	            left join ({$wpdb->term_taxonomy} as tt
	                left join {$wpdb->terms} as t on
	                    tt.term_id = t.term_id
	            ) on
	                tr.term_taxonomy_id = tt.term_taxonomy_id
	        ) on
	            p.ID = tr.object_id
	        where t.term_id = $id
	        limit 1
	    ";
	    $pid = $wpdb->get_var($sql);
	    if (empty($pid)) {
	    	return false;
	    } else {
	        $attachment =  wp_get_attachment_image_src(get_post_thumbnail_id($pid),'size');
	        switch ($return) {
	            case 'url': return $attachment[0]; break;
	            case 'element': return sprintf('<img src="%s" width="%s" height="%s">',$attachment[0],$attachment[1],$attachment[2]); break;
	            default: return $attachment;
	        }
	    }
	    return false;
	}

}