<?php

$rdfw_breadcrumbs = new rdfw_breadcrumbs();

class rdfw_breadcrumbs {

	public static function show() {
		global $post;
		$breadcrumbs = array();
		$breadcrumbs[] = array( 
			'label' => __( 'Acasa', 'rdfw' ),
			'url' => get_bloginfo( 'url' )
		);
		if( is_single() ) {
			if ( is_single( 'post' ) ) {
				$blog_page = rdfw::get_blog_page();
				if ( $blog_page ) {
					$breadcrumbs[] = array( 
						'label' => __( 'Blog', 'rdfw' ),
						'url' => get_permalink( $id, $leavename )
					);
				}
			} else {
				$posttype = get_post_type();
				$parent_page = rdfw::get_page_by_template( sprintf( 'tpl-%s.php', $posttype ) );
				if ( $parent_page ) {
					$breadcrumbs[] = array( 
						'label' => apply_filters( 'the_title', get_the_title() ),
						'url' => get_permalink( $parent_page )
					);
				}
			}
			$breadcrumbs[] = array( 
				'label' => apply_filters( 'the_title', get_the_title() ),
				'url' => get_permalink( get_the_ID() )
			);
		}
		if( is_page() ) {
			$breadcrumbs[] = array( 
				'label' => apply_filters( 'the_title', get_the_title() ),
				'url' => get_permalink( get_the_ID() )
			);
		}
		if ( count( $breadcrumbs ) > 1 ) {
			echo '<ul class="breadcrumbs"';
			foreach ($breadcrumbs as $key => $breadcrumb) {
				printf( '<li><a href="%s">%s</a></li>', $breadcrumb['url'], $breadcrumb['label'] );
			}
			echo '</ul>';
		}
	}

}