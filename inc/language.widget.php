<?php 


$languageWidget = array(
	'name'          => sprintf( __( 'Language Widget' )),
	'id'            => "language_widget",
	'description'   => '',
	'class'         => 'sidebar',
	'before_widget' => '<div class="language-widget">',
	'after_widget'  => "</div>\n",
	'before_title'  => '',
	'after_title'   => "",
);


register_sidebar($languageWidget);