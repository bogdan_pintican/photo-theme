<?php

$radu_options = new radu_options();

class radu_options {

	public function __construct() {
		add_action( 'admin_menu' , array( $this, 'menu' ) );
		add_filter( 'radu_options_group', array( $this, 'contact' ), 10, 1 );
		add_filter( 'radu_options_group', array( $this, 'social' ), 20, 1 );
		add_filter( 'radu_options_group', array( $this, 'others' ), 50, 1 );
	}

	public function contact($groups) {
		$groups[] = array(
			'slug' => 'contact',
			'label' => 'Contact',
		);
		return $groups;
	}

	public function others($groups) {
		$groups[] = array(
			'slug' => 'others',
			'label' => 'Others',
		);
		return $groups;
	}

	public function social($groups) {
		$groups[] = array(
			'slug' => 'social',
			'label' => 'Social',
		);
		return $groups;
	}

	public function menu() {
		add_theme_page('Theme Options', 'Theme Options', 'publish_pages', 'radu-theme-options', array( $this, 'main' ) );
	}

	public function main() {
		if (isset($_POST['radu_options_submit'])) {
			foreach($_POST as $option => $value) {
				if (substr($option,0,5)=='radu_') {
					if ($value=="") {
						delete_option($option);
					} else {
						if (is_array($value)) {
							update_option($option,json_encode($value));
						} else {
							update_option($option,$value);
						}
					}
				}
			}
			$update_message = 'Theme options saved succesfully';
		}
		include TEMPLATEPATH.'/inc/options/main.php';
	}

}