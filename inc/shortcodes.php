<?php

$rdfw_shortocdes = new rdfw_shortcodes();

class rdfw_shortcodes {

	public function __construct() {
		// add_shortcode( 'tab-content', array( $this, 'tab_content' ) );
	}





	public function tab_content( $atts, $content ) {
		extract( shortcode_atts( array(
		), $atts ) );
		if ( !isset( $atts['for'] ) ) return;
		$first = in_array( 'first', $atts ) ? true : false;
		$last = in_array( 'last', $atts ) ? true : false;
		$active = in_array( 'active', $atts ) ? true : false;
		$for = 'accordion-'.$atts['for'];
		$ret = '';
		$ret .= $first ? '<div class="tabs-content">' : '';
		$ret .= sprintf( '<div class="content %s" id="%s">%s</div>', ( $active ? 'active' : '' ), $atts['for'], do_shortcode( $content ) );
		$ret .= $last ? '</div>' : '';
		return $ret;
	}



}