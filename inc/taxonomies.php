<?php

global $radu_taxonomies;
if ( !isset( $radu_taxonomies ) ) $radu_taxonomies = new radu_taxonomies();

class radu_taxonomies {

	public function __construct() {
		// add_action( 'init' , array( $this, 'tip_oferta' ) );
		// add_action( 'init' , array( $this, 'tip_masina' ) );
		// add_action( 'init' , array( $this, 'slider' ) );
	}

	public function tip_oferta() {		
		$labels = array(
			'name'					=> _x( 'Tip Oferta', 'Taxonomy Tip Oferta', 'radu' ),
			'singular_name'			=> _x( 'Tip Oferta', 'Taxonomy Activity', 'radu' ),
			'search_items'			=> __( 'Cauta Tipuri', 'radu' ),
			'popular_items'			=> __( 'Tipuri Populare', 'radu' ),
			'all_items'				=> __( 'Toate Tipurile', 'radu' ),
			'parent_item'			=> __( 'Tip Parinte', 'radu' ),
			'parent_item_colon'		=> __( 'Tip Parinte', 'radu' ),
			'edit_item'				=> __( 'Editeaza', 'radu' ),
			'update_item'			=> __( 'Actualizeaza', 'radu' ),
			'add_new_item'			=> __( 'Adauga', 'radu' ),
			'new_item_name'			=> __( 'Adauga', 'radu' ),
			'add_or_remove_items'	=> __( 'Adauga sau sterge', 'radu' ),
			'choose_from_most_used'	=> __( 'Alege din cele mai folosite', 'radu' ),
			'menu_name'				=> __( 'Tip Oferta', 'radu' ),
		);
		$args = array(
			'labels'            => $labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => true,
			'show_tagcloud'     => true,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug'=>'oferte-speciale' ),
			'query_var'         => true,
			'capabilities'      => array(),
		);
		register_taxonomy( 'tip-oferta', array( 'oferta' ), $args );
	}


	
}