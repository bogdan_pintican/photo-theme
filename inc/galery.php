<?php 

add_image_size( "galery", 260 ); 

add_filter('post_gallery', 'my_post_gallery', 10, 2);

function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    // gets variables and sets defaults
    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
    'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    $output = "<div class=\"gallery-wrapper\">\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
		// $img = wp_get_attachment_image_src($id, 'my-custom-image-size');

        $url = wp_get_attachment_image_src($id, 'full');
        $url = $url[0];
        $thumb = wp_get_attachment_image_src( $id, "galery" );
        $thumb = $thumb[0];


        // print_r($thumb);
        // echo "hello";


        $output .= "<div class='galery-item'>\n";
        $output .= "<a href=" . $url . ">";
        $output .= "<img class='lazy' data-original='" . $thumb . "'>";
        $output .= "</a>";
        $output .= "</div>\n";
       
    }

    $output .= "</div>\n";



    return $output;
}