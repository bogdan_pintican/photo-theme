<?php

global $radu_metaboxes;
if ( !isset( $radu_metaboxes ) ) $radu_metaboxes = new radu_metaboxes;

class radu_metaboxes {

	public function __construct() {
		add_action( 'admin_init', array ( $this, 'init' ) );
		add_action( 'save_post', array ( $this, 'save' ) );
	}

	public function init() {
		// masina
		// add_meta_box( 'radu-masini-pret', 'Pret', array( $this, 'pret' ), 'masina', 'side' );
		add_meta_box( 'radu-partener', 'Partener Url', array( $this, 'partener' ), 'partener', 'normal' );

	}

	public function save( $post_id ) {
		foreach ( $_POST as $key => $value ) {
			if ( substr( $key, 0, 5 )=='radu_' ) {
				if ( empty( $value ) ) {
					delete_post_meta( $post_id, '_'.$key );
				} else {
					if ( is_array( $value ) ) {
						update_post_meta( $post_id, '_'.$key, json_encode( $value ) );
					} else {
						update_post_meta( $post_id, '_'.$key, $value );
					}
				}
			}
		}
	}

	public function model() {
		include_once TEMPLATEPATH.'/inc/metaboxes/model.php';
	}

	public function partener() {
		include_once TEMPLATEPATH.'/inc/metaboxes/partener.php';
	}



}