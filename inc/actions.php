<?php

global $radu_actions;
if ( !isset( $radu_actions ) ) $radu_actions = new radu_actions();

class radu_actions {

	public function __construct() {
		// theme setup
		add_action( 'after_setup_theme', array( $this, 'setup' ) );
		// widgets
		add_action( 'widgets_init', array( $this, 'widgets' ) );
		// admin scripts
		add_action( 'admin_enqueue_scripts', 'wp_enqueue_media' );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
		// admin styles
		add_action( 'admin_print_styles', array( $this, 'admin_styles' ) );
		// scripts 
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ) );
		// styles
		add_action( 'wp_head', array( $this, 'styles' ) );
		// remove editor
		add_action( 'init', array( $this, 'remove_editor' ) );
		//roles
		add_action( 'init', array( $this, 'roles' ) );
		// attachments
		add_action( 'attachments_register', array( $this, 'attachments' ) );
	}

	public function roles() {
		global $wp_roles;
		$wp_roles->add_role('expert', 'Expert', array('read'));
		//$admin = $wp_roles->get_role('administrator');
	}

	public function profile_fields( $user ) {
?>
<h3>Extra profile information</h3>
<table class="form-table">
	<tr>
		<th><label for="twitter">Twitter</label></th>
		<td>
			<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
			<span class="description">Please enter your Twitter username.</span>
		</td>
	</tr>

</table>
<?php 
	}

	public function remove_editor() {
		global $_wp_post_type_features;
		if ( !isset($_GET['post'] ) ) return;
		$post = $_GET['post'];
		$post_type = get_post_type( $post );
		//echo '<pre>'; var_dump($_wp_post_type_features); die();
		/*$template = get_post_meta( $_GET['post'], '_wp_page_template', true );
		if (in_array( $template, array('tpl-services.php','tpl-links.php') ) )
			unset( $_wp_post_type_features['page']['editor'] );*/
		/*if ( rdfw::is_set_home( $post ) )
			unset( $_wp_post_type_features['page']['editor'] );*/
		if ( $post_type == 'masina') {
			unset( $_wp_post_type_features['post']['editor'] );
			unset( $_wp_post_type_features['page']['editor'] );
		}
	}

	public function setup() {
		add_editor_style();
		add_theme_support( 'woocommerce' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		register_nav_menus( array(
			'primary' => 'Meniu Principal',
			// 'top' => 'Meniu Sus',
			// 'footer' => 'Meniu Footer',
		) );
	}

	public function widgets() {
		register_sidebar( array(
			'name' => 'Sidebar with border',
			'id' => 'sidebar',
			'description' => 'Sidebar Widgets',
			'before_widget' => '<div class="widget white-box">',
			'after_widget' => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		) );
	}

	public function admin_scripts() {
		wp_enqueue_script( 'radu-admin-uploader', get_bloginfo( 'template_url' ).'/assets/media-uploader.js', array( 'jquery' ), '1.0');
		wp_enqueue_script( 'jquery-ui-core' );
		// wp_enqueue_script( 'jquery-ui-sortable' );
		// wp_enqueue_script( 'jquery-ui-widget' );
		// wp_enqueue_script( 'jquery-ui-mouse' );
		//wp_enqueue_script( 'jquery-ui-tooltip' );
		//wp_enqueue_script( 'jquery-ui-datepicker' );
		///wp_enqueue_script( 'iris' );
		// wp_enqueue_script( 'radu-admin', get_bloginfo( 'template_url' ).'/assets/admin.js', array( 'jquery' ), '1.0');
	}

	public function admin_styles() {
		wp_enqueue_style( 'radu-admin-styles', get_bloginfo( 'template_url' ).'/assets/admin.css', array(), '1.0', 'all' );
		wp_enqueue_style( 'radu-admin-jqueryui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css', array(), '1.0', 'all' );

	}

	public function scripts() {
		// wp_enqueue_script('jquery-ui-core');
		// wp_enqueue_script('jquery-effects-core');
		wp_enqueue_script('jquery-masonry');
		// wp_enqueue_script( 'jquery-ui-core' );
		// wp_enqueue_script( 'jquery-ui-sortable' );
		// wp_enqueue_script( 'jquery-ui-widget' );
		// wp_enqueue_script( 'jquery-ui-mouse' );

	}

	public function styles() {
		//printf( '<link rel="stylesheet" type="text/css" href="%s?v=%s">', get_bloginfo( 'template_url' ).'/bundles/fancybox/jquery.fancybox.css', '2.1.5' );
	}

	public function attachments( $attachments ) {
		$fields				 = array(
			array(
				'name'			=> 'title',												 // unique field name
				'type'			=> 'text',													// registered field type
				'label'		 => __( 'Title', 'attachments' ),		// label to display
				'default'	 => 'title',												 // default value upon selection
			),
		);

		$args = array(
			'label'				 => 'Galerie',
			'post_type'		 => array( 'masina' ),
			'position'			=> 'normal',
			'priority'			=> 'high',
			'filetype'			=> array('image'),
			'note'					=> 'Adauga imagini!',
			'append'				=> true,
			'button_text'	 => __( 'Adauga imagini', 'attachments' ),
			'modal_text'		=> __( 'Adauga', 'attachments' ),
			'router'				=> 'cauta',
			'post_parent'	 => false,
			'fields'				=> $fields,

		);

		$attachments->register( 'my_attachments', $args ); // unique instance name
	}



}