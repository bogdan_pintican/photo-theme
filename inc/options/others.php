<table class="form-table">	
	<tr>
		<th>Custom CSS</th>
		<td><textarea name="radu_custom_css" rows="7" style="width:25em;"><?php echo stripslashes(get_option('radu_custom_css')) ?></textarea></td>
	</tr>	
	<tr>
		<th>Logo</th>
		<td><input name="radu_logo" rows="7" type="text" value="<?php echo get_option('radu_logo') ?>"></td>
	</tr>
	<tr>
		<th>Google Analytics Code</th>
		<td><textarea name="radu_google_analytics" rows="7" style="width:25em;"><?php echo stripslashes(get_option('radu_google_analytics')) ?></textarea></td>
	</tr>
</table>