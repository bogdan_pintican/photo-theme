<table class="form-table">	
	<tr>
		<th><h3>Social Media</h3></th>
	</tr>
	<?php 
		$social = array(
			'facebook'=> 'Facebook',
			'twitter'=>'Twitter',
			'pinterest'=>'Pinterest',
			'linkedin'=>'LinkedIn',
			'google-plus'=>'Google+',
			'tumblr'=>'Tumblr',
			'instagram'=>'Instagram',
			'youtube'=>'Youtube',
			'deviantart'=>'Deviantart',
			'500px'=>'500px',
		);
		foreach ($social as $slug => $label):
	?>
	<tr>
		<th><?php echo $label ?></th>
		<td><input type="text" name="radu_social_url_<?php echo $slug ?>" class="regular-text" value="<?php echo get_option('radu_social_url_'.$slug) ?>" /></td>
	</tr>
	<?php endforeach; ?>
</table>