<?php $groups = apply_filters( 'radu_options_group', array() ); ?>
<div class="wrap">
	<h2>Theme Options</h2>
	<?php if (isset($update_message)): ?>
	<div class="updated"><p><?php echo $update_message ?></p></div>
	<?php endif; ?>
	<h2 class="nav-tab-wrapper">
		<?php 
			foreach( $groups as $key => $group ) {
				printf( '<a href="#%s" class="nav-tab radu-options-tab">%s</a>', $group['slug'], $group['label'] );
			}
		?>
	</h2>
	<form action="" method="post">
		<?php foreach( $groups as $key => $group): ?>
		<div id="<?php echo $group['slug'] ?>" class="radu-options-box">
			<?php include TEMPLATEPATH.'/inc/options/'.$group['slug'].'.php'; ?>
		</div>
		<?php endforeach; ?>
		<p class="submit"><input type="submit" class="button-primary" name="radu_options_submit" value="Save Options" /></p>
	</form>
</div>