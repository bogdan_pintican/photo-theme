<table class="form-table">	
	<tr><th colspan="2"><h3>Contact Details</h3></th></tr>
	<tr>
		<th>Phone</th>
		<td><input type="text" name="radu_contact_phone" class="regular-text" value="<?php echo get_option('radu_contact_phone') ?>" /></td>
	</tr>
	<tr>
		<th>Email</th>
		<td><input type="text" name="radu_contact_email" class="regular-text" value="<?php echo get_option('radu_contact_email') ?>" /></td>
	</tr>
	<tr>
		<th>Address</th>
		<td><textarea name="radu_contact_address" style="width:25em;"><?php echo stripslashes(get_option('radu_contact_address')) ?></textarea></td>
	</tr>
	<tr><th colspan="2"><h3>Mailchimp</h3></th></tr>
	<tr>
		<th>API Key</th>
		<td><input type="text" class="regular-text" name="radu_mailchimp_api" value="<?php echo get_option('radu_mailchimp_api') ?>"></td>
	</tr>
	<tr>
		<th>List ID</th>
		<td><input type="text" class="regular-text" name="radu_mailchimp_list" value="<?php echo get_option('radu_mailchimp_list') ?>"></td>
	</tr>
</table>