<?php

global $radu_posttypes;
if ( !isset( $radu_posttypes ) ) $radu_posttypes = new radu_posttypes();

class radu_posttypes {

	public function __construct() {
		add_action( 'init' , array( $this, 'partener' ) );
	}

	public function partener() {
		$labels = array(
			'name'                => __( 'Partener', 'radu' ),
			'singular_name'       => __( 'Partener', 'radu' ),
			'add_new'             => _x( 'Adauga partener', 'radu', 'radu' ),
			'add_new_item'        => __( 'Adauga partener', 'radu' ),
			'edit_item'           => __( 'Editeaza Partener', 'radu' ),
			'new_item'            => __( 'Partener Nou', 'radu' ),
			'view_item'           => __( 'Vezi Partener', 'radu' ),
			'search_items'        => __( 'Cauta partener', 'radu' ),
			'not_found'           => __( 'Nici un partener', 'radu' ),
			'not_found_in_trash'  => __( 'Nici un partener', 'radu' ),
			'parent_item_colon'   => __( 'Parteneri:', 'radu' ),
			'menu_name'           => __( 'Parteneri', 'radu' ),
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => '',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => null,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => false,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'thumbnail', 'page-attributes'
			)
		);
		register_post_type( 'partener', $args );
	}



}