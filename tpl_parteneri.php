<?php 
/*
Template Name: Parteneri Page
*/
?>

<?php get_header() ?>


<?php the_post() ?>

<div class="main-content">
			<div class="row partener">
			<?php
			$args = array(
				'post_type'=>'partener',
				'posts_per_page'=>-1,
				'order'=>'menu_order');
			$all_pages = new WP_Query($args);
			$i=0;	
			while ($all_pages->have_posts()): $all_pages->the_post(); ?>
				<div class="medium-3 columns">
					<a target="blank" href="<?php echo get_post_meta( $post->ID, '_radu_parteneri', true ); ?>">
						<?php the_post_thumbnail(); ?>
					</a>
				</div>
			<?php 	endwhile; wp_reset_query(); ?>
			</div>

</div><!-- Main Content -->



<?php get_footer() ?>