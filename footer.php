		</div>
		<footer>
			<hr>
			<p class="footer-copyright">Copyright &copy; <?= date('Y') ?><i> <a href="<?php bloginfo("url") ?>"><?php bloginfo('name') ?></a>. </i> Toate drepturile rezervate.</p>
			<p class="footer-copyright">Built by <a target="_blank" href="http://bogdanpetru.eu">Bogdan Petru Pintican</a></p>
		</footer>
		<?php wp_footer() ?>
		<!-- Facebook -->
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		<script src="<?php echo bloginfo("template_url"); ?>/js/vendor/lazyLoad.js"></script>
		<script src="<?php echo bloginfo("template_url"); ?>/js/vendor/lightBox.js"></script>
		<!-- Magnifc -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url" ); ?>/css/magnific.css">
		<script src="<?php echo bloginfo("template_url" ); ?>/js/magnific.js"></script>
		<script src="<?php echo bloginfo("template_url" ); ?>/js/foundation/foundation.js"></script>
		<script src="<?php echo bloginfo("template_url"); ?>/js/main.js"></script>
		<div id="fb-root"></div>
	</body>
</html>
