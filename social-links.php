<?php

$social = array(
	'facebook'=> 'Facebook',
	'twitter'=>'Twitter',
	'pinterest'=>'Pinterest',
	'google-plus'=>'Google+',
	'linkedin'=>'LinkedIn',
	'tumblr'=>'Tumblr',
	'instagram'=>'Instagram',
	'youtube'=>'Youtube',
	'deviantart'=>'Deviantart',
	'500px'=>'500px',
);
foreach ($social as $slug => $label) {
	$url = get_option('radu_social_url_'.$slug);
	if (!empty($url))
		printf('<a target="blank" href="%s" title="%s"><span class="fa fa-%s text-color"></span></a>',$url,$label,$slug);
}